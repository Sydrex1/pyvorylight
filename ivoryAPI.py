import ivoryHAL
import struct

class ivoryAPI:

    _errorCode = [
        'success',
        'errBadFormat',
        'errDataTooLong',
        'errAesDataLenError',
        'errAesDefaultError',
        'errBurnAlreadyBurn',
        'errBurnDefaultError',
        'errColorPinTooManyDigits',
        'errDrbgPersoStrOverflow',
        'errDrbgDefaultError',
        'errFramAccessFailed',
        'errHmacTagTooLong',
        'errKdfDefaultError',
        'errKeyboardNotSet',
        'errKeyboardSequenceNotValid',
        'errSha2LengthLimitExceeded',
        'errSha2DefaultError',
        'errOtpNotBurn',
        'errMasterKeyNotLoad',
        'errDerivatedKeysNotLoad',
        'errUnknownCommand',
        'errUnknown'	
    ]

    def __init__(self,phyLayerMode = "local", defaultTimeOut = 1, listenPort = 10000, verbose = False, bank0isReadOnly = True):
        assert(phyLayerMode in [ "local", "remote"]), "compliant physical layer are 'local' or 'remote'"
        assert(defaultTimeOut>0 and defaultTimeOut<10), "Default timeout is a value in range of 1s to 10s"
        assert(phyLayerMode == "local" or ( phyLayerMode == "remote" and (listenPort>128 and listenPort<65536))), "In 'remote' mode, listen port muts be a value between 129 and 65535"        
        if(phyLayerMode=="local"):
            self._hal = ivoryHAL.IvorySerialHAL(defaultTimeOut=defaultTimeOut, verbose = verbose)  # open serial port
        else:
            self._hal = ivoryHAL.IvoryTcpHAL(defaultTimeOut=defaultTimeOut, listenPort = listenPort, verbose = verbose)  # open server and waiting for a connection
        self._verbose = verbose
        self._defaultTimeOut = defaultTimeOut
        self._bank0isReadOnly = bank0isReadOnly

    def isConnected(self):
        return self._hal.isConnected()

    def _sendFrame(self,  cmdParams, dataPayload):
        defaultTimeOut = 1
        (cmdName, cmdTimeOut, tgtPayloadLen, tgtAnsLen) = cmdParams
        assert(len(dataPayload) == tgtPayloadLen), "Invalid input frame length"
        # Init
        self._hal.flush()
        # Frame length
        lenMSB = len(dataPayload) // 256 
        lenLSB = len(dataPayload)  % 256 
        assert(lenMSB * 256 + lenLSB == len(dataPayload)), "Frame length error"
        # subFrame 
        subFrame = bytes( [lenMSB, lenLSB ]) + dataPayload
        acc = 0
        for b in subFrame:
            acc += int(b)
        checksum = bytes([acc % 256])
        txFrame = b'\x02' + subFrame + checksum + b'\x03' 
        self._hal.write(txFrame)
        # Listen anwser
        rxACQ = self._hal.read(1)
        if(rxACQ==b'\x15'):
            # Negative ACK
            raise BufferError(cmdName + " : Invalid command")
        elif(rxACQ==b'\x06'):
            # Positive ACK
            header = self._hal.read(1,cmdTimeOut)
            if(header!=b'\x02'):
                raise BufferError( cmdName + " : Invalid header : " + str(header) )
            rxStatus = self._hal.read(1)
            lenMSB = self._hal.read(1)
            lenLSB = self._hal.read(1)
            if(rxStatus==b'\x12'):
                statusCode = self._hal.read(1)
                dataPayload = statusCode
            elif(rxStatus==b'\x11'):
                statusCode = b'\x00'
                length = 256 * int.from_bytes(lenMSB,"big") + int.from_bytes(lenLSB,"big")
                dataPayload = self._hal.read(length)
            else:
                raise BufferError(cmdName + " : Invalid rxStatus flag : " + str(rxStatus))
            # Compute local cheksum
            subFrame = lenMSB + lenLSB + dataPayload
            acc = 0
            for b in subFrame:
                acc += int(b)
            local_checksum = bytes([acc % 256])
            rx_checksum   = self._hal.read(1)
            if(local_checksum!=rx_checksum):
                raise BufferError(cmdName + " : Invalid cheksum Rx: " + str(rx_checksum) + " Computed:" + str(local_checksum))
            terminator = self._hal.read(1)
            if(terminator!=b'\x03'):
                raise BufferError(cmdName + " : Invalid terminator : " + str(terminator) )
            if(tgtAnsLen != len(dataPayload) and rxStatus!=b'\x12'):
                raise BufferError(cmdName + " : Invalid answer length " + str(len(dataPayload)) + " expected " +  str(tgtAnsLen))
            return (self._errorCode[int.from_bytes(statusCode,"big")], dataPayload)
        else:
            raise BufferError(cmdName + " : Invalid ACK : " + str(rxACQ) )

#   Command name                              Code      InLen OutLen
    _cmdSystem = {
        "NULL"					        :	( b'\x00',     1,      0),
        "ECHO"				    	    :	( b'\x01',    -1,     -1),
        "INFO"					        :	( b'\x02',     1,      3),
        "TSTLED"				        :	( b'\x03',     2,      0),  
        "SET_TIMEOUT"			        :	( b'\x04',     2,      0),
        "OFF"					        :	( b'\x0F',     1,      0)
    }

    _cmdFRAM = {
        "FRAM_READ"			            :  ( b'\x11',      5,      -1),
        "FRAM_WRITE"			        :  ( b'\x12',      -1,      0)
    }

    _cmdKeyboard = {
        "SET_SEQUENCE"		            :  ( b'\x21',      -1,     1),
        "GET_SEQUENCE"		            :  ( b'\x22',       1,     0),
        "SET_MODE"			            :  ( b'\x23',       2,     0),
        "GET_MODE"			            :  ( b'\x24',       1,     1)
    }

    _cmdRandomGenerator = {
        "RAW_SEED"		                :  ( b'\x71',      1,    128),
        "INSTANCIATE"		            :  ( b'\x72',     81,      0),
        "GENERATE"		                :  ( b'\x73',     67,     -1), 
        "HEALTH_TEST"		            :  ( b'\x74',      1,      9)
    }

    _cmdCrypto = {
        "AES_ECB_ENC"			        :  ( b'\x81',      -1,     -1),
        "AES_ECB_DEC"                   :  ( b'\x82',      -1,     -1),
        "AES_CBC_ENC"			        :  ( b'\x83',      -1,     -1),
        "AES_CBC_DEC"			        :  ( b'\x84',      -1,     -1),
        "SHA2_256"			            :  ( b'\x91',      -1,     32),
        "SHA2_512"			            :  ( b'\x92',      -1,     64),
        "HMAC_SHA2_256"		            :  ( b'\xA1',      -1,     -1),
        "HMAC_SHA2_512"		            :  ( b'\xA2',      -1,     -1),
        "KDF_HMAC_SHA2_256"	            :  ( b'\xB1',      -1,     -1),
        "KDF_HMAC_SHA2_512"	            :  ( b'\xB2',      -1,     -1)
    }

    _cmdSecuredKey = {
        "INIT_MK_WITH_PSWD"	            :  ( b'\xC1',      33,      0),
        "INIT_MK_WITH_PIN"	            :  ( b'\xC2',       1,      0),
        "INIT_MK_STD_ALONE"	            :  ( b'\xC3',       1,      0),
        "SET_PIN_INIT"		            :  ( b'\xC4',       1,      0),      
        "SET_PIN_NEXTDIGIT"	            :  ( b'\xC5',       2,      0),
        "GET_SEED"			            :  ( b'\xC8',       1,     32),
        "SET_AESHMAC_KEY"		        :  ( b'\xC9',      -1,      0),
        "NEW_EXT_AESHMAC_KEY"	        :  ( b'\xCA',      -1,     96)
    }

    _cmdSecuredCrypto = {
        "AES_ECB_ENC_IK"		        :  ( b'\xD1',      -1,     -1),
        "AES_ECB_DEC_IK"		        :  ( b'\xD2',      -1,     -1),      
        "AES_CBC_ENC_IK"		        :  ( b'\xD3',      -1,     -1),
        "AES_CBC_DEC_IK"		        :  ( b'\xD4',      -1,     -1),
        "HMAC_SHA2_256_IK"  	        :  ( b'\xE1',      -1,     32),
        "HMAC_SHA2_512_IK"	            :  ( b'\xE2',      -1,     64)
    }

    _cmdSecuredMisc = {
        "GENERATE_CLEAR_PASSWORD"       : ( b'\xF1',       3,      -1),
        "GENERATE_ENCRYPTED_PASSWORD"   : ( b'\xF2',       3,      56),
        "TYPE_ENCRYPTED_PASSWORD"       : ( b'\xF3',      57,       1),
        "CHECK_FIRMWARE"		        : ( b'\xF4',      -1,      -1),
    #       "BURN_SECRETS"		            : ( b'\xF5',       1,        0),
        "HASH_FIRMWARE"		            : ( b'\xF6',       2,      -1)
    }

    _commands = {
        "System"                :   _cmdSystem,
        "FRAM"                  :   _cmdFRAM,
        "Keyboard"              :   _cmdKeyboard,
        "Random Generator"      :   _cmdRandomGenerator,
        "Cryptography"          :   _cmdCrypto,
        "Secured Key"           :   _cmdSecuredKey,
        "Secured Cryptography"  :   _cmdSecuredCrypto,
        "Secured Misc"          :   _cmdSecuredMisc
    }


    def systemNullCmd(self):
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["System"]["NULL"]
        cmdParams = ("System null", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )

    def systemEchoCmd(self,msg):
        assert(len(msg)<2048), "Message must be shorter than 2048 bytes"
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["System"]["ECHO"]
        dataPayload += msg 
        tgtPayloadLen = len(msg) + 1
        tgtAnsLen     = len(msg)
        cmdParams = ("System echo", self._defaultTimeOut + 3, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )

    def systemInfoCmd(self):
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["System"]["INFO"]
        cmdParams = ("System info", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        (status, ret) = self._sendFrame(cmdParams, dataPayload)
        out = int(ret[0]), int(ret[1]), ret[2]==b'\x00'
        return( status, out )

    def systemLEDTestCmd(self, duration):
        assert(duration>0 and duration<2.5), "Duration must be between 0s and 2.5s"
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["System"]["TSTLED"]
        dataPayload += bytearray([int(duration*100)])
        cmdParams = ("System LED test", duration + 0.5, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return ( status , ret )
        
    def systemSetTimeoutCmd(self,timeout):
        assert(timeout>=30 and timeout<=2580), "Timeout must be between 30s and 2580s"
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["System"]["SET_TIMEOUT"]
        dataPayload += bytearray([int((timeout-30)/10)])
        cmdParams = ("System set timeout", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return ( status , ret )
        
    def systemOFFCmd(self):
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["System"]["OFF"]
        cmdParams = ("System OFF", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )

    def FRAMreadCmd(self,bank,address,size):
        assert(bank in [0,1]), "Valid banks are 0 or 1"
        assert(size > 0 and size < 256), "Size must by between 1 and 255" 
        assert(address>=0 and address<65536), "Valid address are between 0 and 65535"
        assert(address+size-1 < 65536), "Read out of memory limit"
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["FRAM"]["FRAM_READ"]
        tgtAnsLen = size
        dataPayload += bytearray([bank])
        dataPayload += bytearray( struct.pack('!H',address) )
        dataPayload += bytearray([size])
        cmdParams = ("FRAM read", size/100, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )

    def FRAMwriteCmd(self,bank,address,data):
        assert(bank in [0,1]), "Valid banks are 0 or 1"
        assert( bank==1 or (bank==0 and self._bank0isReadOnly==False)), "Bank 0 is write protected"
        assert(len(data) > 0 and len(data) < 256), "Data length must by between 1 and 255" 
        assert(address>=0 and address<65536), "Valid address are between 0 and 65535"    
        assert(address+len(data)-1 < 65536), "Read out of memory limit"
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["FRAM"]["FRAM_WRITE"]
        tgtPayloadLen = 5 + len(data)
        dataPayload += bytearray([bank])
        dataPayload += bytearray( struct.pack('!H',address) )
        dataPayload += bytearray([len(data)])
        dataPayload += data
        cmdParams = ("FRAM write", len(data)/100, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )
    
    def keyboardSetSequenceCmd(self,sequence):
        sequence += b'\x00'
        assert(len(sequence)>0 and len(sequence)<=64), "Key sequence length must be between 1 and 64"
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Keyboard"]["SET_SEQUENCE"]
        tgtPayloadLen = 1 + len(sequence)
        dataPayload += sequence
        cmdParams = ("Keyboard set sequence", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )

    def keyboardGetSequenceCmd(self):
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Keyboard"]["GET_SEQUENCE"]
        cmdParams = ("Keyboard get sequence", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )
    
    def keyboardSetModeCmd(self, mode):
        assert(mode in ["azerty", "qwerty"]), "Compliant keyboard modes are 'azerty' and 'qwerty'"
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Keyboard"]["SET_MODE"]
        if(mode=="azerty"):
            dataPayload += bytearray([1])
        else:
            dataPayload += bytearray([0])
        cmdParams = ("Keyboard set mode", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )
    
    def keyboardGetModeCmd(self):
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Keyboard"]["GET_MODE"]
        cmdParams = ("Keyboard get mode", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        if(int(ret[0]) == 1):
            out = "azerty"
        else:
            out = "qwerty" 
        return( status , out )

    def randomGeneratorGetRawSeedCmd(self):
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Random Generator"]["RAW_SEED"]
        cmdParams = ("Random generator get raw seed", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )

    def randomGeneratorInstanciateCmd(self,entropySrc,nonce):
        assert(len(entropySrc)==64), "Entropy source length must be equal to 64"
        assert(len(nonce)==16), "nonce length must be equal to 16"
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Random Generator"]["INSTANCIATE"]
        dataPayload += entropySrc
        dataPayload += nonce
        cmdParams = ("Random generator instanciate", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )
    
    def randomGeneratorGenerateCmd(self,size,entropySeed):
        assert(size>0 and size<=2048), "Requested size must be between 1 and 2048"
        assert(len(entropySeed)==64), "Entropy seed length must be equal to 64"
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Random Generator"]["GENERATE"]
        dataPayload += bytearray( struct.pack('!H',int(size)) )
        dataPayload += entropySeed
        tgtAnsLen = int(size)
        cmdParams = ("Random generator generate", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )

    def randomGeneratorHealthTestCmd(self):
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Random Generator"]["HEALTH_TEST"]
        cmdParams = ("Random generator health test", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        print(ret)
        repetitionBitCountTest = struct.unpack('>I', ret[0:4])[0]
        adaptiveProportionTest = struct.unpack('>I', ret[4:8])[0]
        testSuccess = (ret[8] == b'\x00')
        out = ( repetitionBitCountTest, adaptiveProportionTest, testSuccess)
        return( status , out )

    def __cryptoAESgenericCmd(self,keyLen,key,iv,data,cmd):
        assert(cmd in ["AES_ECB_ENC", "AES_ECB_DEC", "AES_CBC_ENC", "AES_CBC_DEC"]), "Invalid command"
        assert(keyLen in ["128 bits","256 bits"]), "AES keymode len must be '128 bits' or '256 bits'"        
        if(keyLen=="128 bits"):
            assert(len(key)==16),   "AES128 key length must be equal to 16"
            keyCmd = b'\x01'
            cmdName = "AES 128 "
        else:
            assert(len(key)==32),   "AES256 key length must be equal to 32"
            keyCmd = b'\x02'
            cmdName = "AES 256 "
        if(iv==None):
            iv = bytearray(16)
        assert(len(iv)==16),    "IV length must be equal to 16"
        assert(len(data)<2048), "Data length maximum is 2048"
        assert(len(data)%16==0), "Data length must be a multiple of 16"
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Cryptography"][cmd]
        dataPayload += keyCmd
        dataPayload += key
        # Zero padding for AES128bits key
        if(len(key)==16):
            dataPayload += bytearray(16)
        dataPayload += iv
        dataPayload += data
        tgtAnsLen = len(data)
        tgtPayloadLen = 2 + 32 + 16 + len(data)
        cmdNameInfo = {
            "AES_ECB_ENC" : "ECB encryption", 
            "AES_ECB_DEC" : "ECB decryption", 
            "AES_CBC_ENC" : "CBC encryption", 
            "AES_CBC_DEC" : "CBC decryption"
        }        
        cmdName += cmdNameInfo[cmd]
        cmdParams = (cmdName, self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )

    def cryptoAES128forECBencryptionCmd(self,key,data):
        return(self.__cryptoAESgenericCmd("128 bits",key,None,data,"AES_ECB_ENC"))

    def cryptoAES256forECBencryptionCmd(self,key,data):
        return(self.__cryptoAESgenericCmd("256 bits",key,None,data,"AES_ECB_ENC"))

    def cryptoAES128forECBdecryptionCmd(self,key,data):
        return(self.__cryptoAESgenericCmd("128 bits",key,None,data,"AES_ECB_DEC"))

    def cryptoAES256forECBdecryptionCmd(self,key,data):
        return(self.__cryptoAESgenericCmd("256 bits",key,None,data,"AES_ECB_DEC"))

    def cryptoAES128forCBCencryptionCmd(self,key,iv,data):
        return(self.__cryptoAESgenericCmd("128 bits",key,iv,data,"AES_CBC_ENC"))

    def cryptoAES256forCBCencryptionCmd(self,key,iv,data):
        return(self.__cryptoAESgenericCmd("256 bits",key,iv,data,"AES_CBC_ENC"))

    def cryptoAES128forCBCdecryptionCmd(self,key,iv,data):
        return(self.__cryptoAESgenericCmd("128 bits",key,iv,data,"AES_CBC_DEC"))

    def cryptoAES256forCBCdecryptionCmd(self,key,iv,data):
        return(self.__cryptoAESgenericCmd("256 bits",key,iv,data,"AES_CBC_DEC"))


    def __cryptoSHAgenericCmd(self,data,mode):
        assert(mode in ["256", "512"]), "Only compliant with SHA256 and SHA512"
        assert(len(data)>1 and len(data)<14336), "Data length maximum is 14336"
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Cryptography"]["SHA2_"+mode]
        tgtPayloadLen = len(data) + 1
        dataPayload += data
        cmdParams = ("SHA "+mode+" ", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )

    def cryptoSHA256Cmd(self,data):
        return(self.__cryptoSHAgenericCmd(data,"256"))

    def cryptoSHA512Cmd(self,data):
        return(self.__cryptoSHAgenericCmd(data,"512"))

    def __cryptoHMACgenericCmd(self,data,key,tagLen,mode):
        assert(mode in ["SHA2_256", "SHA2_512"]), "Only compliant with 'SHA2_256' and 'SHA2_512'"
        assert(len(key)>0 and len(key)<=1024), " Key length must be between 1 and 1024"
        assert(len(data)>0 and len(data)<=14336), " Data length must be between 1 and 14336"
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Cryptography"]["HMAC_"+mode]
        tgtPayloadLen = 7 + len(key) + len(data)     
        dataPayload += bytearray( struct.pack('!H', len(key)  ) )
        dataPayload += bytearray( struct.pack('!H', len(data) ) )
        dataPayload += bytearray( struct.pack('!H', tagLen    ) )
        dataPayload += key
        dataPayload += data
        tgtAnsLen = tagLen
        cmdParams = ("HMAC " + mode + " ", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )

    def cryptoHMACSHA256genericCmd(self,data,key,tagLen):
        return(self.__cryptoHMACgenericCmd(data,key,tagLen,"SHA2_256"))

    def cryptoHMACSHA512genericCmd(self,data,key,tagLen):
        return(self.__cryptoHMACgenericCmd(data,key,tagLen,"SHA2_512"))

    def __cryptoKDFgenericCmd(self,keyIn,fixedInputData,keyOutLen,mode):
        assert(mode in ["HMAC_SHA2_256", "HMAC_SHA2_512"]), "Only compliant with 'HMAC_SHA2_256' and 'HMAC_SHA2_512'"
        assert(len(keyIn)>0 and len(keyIn)<=1024), " Key length must be between 1 and 1024"
        assert(len(fixedInputData)>0 and len(fixedInputData)<=1024), " Fixed input data length must be between 1 and 1024"
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Cryptography"]["KDF_"+mode]
        tgtPayloadLen = 7 + len(keyIn) + len(fixedInputData)
        dataPayload += bytearray( struct.pack('!H', len(keyIn) ) )
        dataPayload += bytearray( struct.pack('!H', len(fixedInputData) ) )
        dataPayload += bytearray( struct.pack('!H', keyOutLen  ) )
        dataPayload += keyIn
        dataPayload += fixedInputData
        tgtAnsLen = keyOutLen
        cmdParams = ("HMAC " + mode + " ", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )

    def cryptoKDFHMACSHA256Cmd(self,keyIn,fixedInputData,keyOutLen):
        return(self.__cryptoKDFgenericCmd(keyIn,fixedInputData,keyOutLen,"HMAC_SHA2_256"))

    def cryptoKDFHMACSHA512Cmd(self,keyIn,fixedInputData,keyOutLen):
        return(self.__cryptoKDFgenericCmd(keyIn,fixedInputData,keyOutLen,"HMAC_SHA2_512"))

    def securedKeyPasswordMKinitCmd(self,password):
        assert(len(password)>0 and len(password)<=32), "Password bad length"
        while(len(password)<32):
            password += b'\x00'            
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Secured Key"]["INIT_MK_WITH_PSWD"]
        dataPayload += password
        cmdParams = ("Init MK with password", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )

    def securedKeyStandAloneMKinitCmd(self):
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Secured Key"]["INIT_MK_STD_ALONE"]        
        cmdParams = ("Init MK stand alone", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )

    def securedKeyInitPINcodeCmd(self):
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Secured Key"]["SET_PIN_INIT"]        
        cmdParams = ("Start PIN code", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )

    def securedKeySetDigitPINcodeCmd(self, digit):
        assert(digit>=0 and digit<10), "Digit must be between 0 and 9"
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Secured Key"]["SET_PIN_NEXTDIGIT"]
        dataPayload += bytearray([digit])
        cmdParams = ("Input digit PIN code", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )

    def securedKeyFinalisePINcodeCmd(self):
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Secured Key"]["INIT_MK_WITH_PIN"]
        cmdParams = ("Finalize PIN code", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )

    def securedKeyGetSeedCmd(self):
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Secured Key"]["GET_SEED"]        
        cmdParams = ("Init MK stand alone", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )

    def securedKeyAesHmacKeysSetupCmd(self,label,seed):
        assert(len(label)<=27), "Label maximum length is 27"
        assert(len(seed)==32), "Seed length must be 32"       
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Secured Key"]["SET_AESHMAC_KEY"]
        tgtPayloadLen = 34 + len(label)
        dataPayload += bytearray([len(label)])
        dataPayload += seed      
        dataPayload += label                
        cmdParams = ("Set AES HMAC external keys", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )
    
    def securedKeyGenerateAndExtractUniqueAesHmacKeysCmd(self,label):
        assert(len(label)<=27), "Label maximum length is 27"  
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Secured Key"]["NEW_EXT_AESHMAC_KEY"]
        tgtPayloadLen = 2 + len(label)
        dataPayload += bytearray([len(label)])     
        dataPayload += label
        cmdParams = ("Generate AES HMAC external keys", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        genSeed    = ret[00:32] 
        genKeyAES  = ret[32:64]
        genKeyHMAC = ret[64:96] 
        out = (genSeed, genKeyAES, genKeyHMAC)       		
        return( status , out )

    def __securedAESgenericCmd(self,keyLen,iv,data,cmd):
        assert(cmd in ["AES_ECB_ENC_IK", "AES_ECB_DEC_IK", "AES_CBC_ENC_IK", "AES_CBC_DEC_IK"]), "Invalid command"
        assert(keyLen in ["128 bits","256 bits"]), "AES keymode len must be '128 bits' or '256 bits'"        
        if(keyLen=="128 bits"):
            keyCmd = b'\x01'
            cmdName = "Secured AES 128 "
        else:
            keyCmd = b'\x02'
            cmdName = "Secured AES 256 "
        if(iv==None):
            iv = bytearray(16)
        assert(len(iv)==16),    "IV length must be equal to 16"
        assert(len(data)<2048), "Data length maximum is 2048"
        assert(len(data)%16==0), "Data length must be a multiple of 16"
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Secured Cryptography"][cmd]
        dataPayload += keyCmd
        dataPayload += iv
        dataPayload += data
        tgtAnsLen = len(data)
        tgtPayloadLen = 2 + 16 + len(data)
        cmdNameInfo = {
            "AES_ECB_ENC_IK" : "ECB encryption", 
            "AES_ECB_DEC_IK" : "ECB decryption", 
            "AES_CBC_ENC_IK" : "CBC encryption", 
            "AES_CBC_DEC_IK" : "CBC decryption"
        }        
        cmdName += cmdNameInfo[cmd]
        cmdParams = (cmdName, self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )

    def securedAES128forECBencryptionCmd(self,data):
        return(self.__securedAESgenericCmd("128 bits",None,data,"AES_ECB_ENC_IK"))

    def securedAES256forECBencryptionCmd(self,data):
        return(self.__securedAESgenericCmd("256 bits",None,data,"AES_ECB_ENC_IK"))

    def securedAES128forECBdecryptionCmd(self,data):
        return(self.__securedAESgenericCmd("128 bits",None,data,"AES_ECB_DEC_IK"))

    def securedAES256forECBdecryptionCmd(self,data):
        return(self.__securedAESgenericCmd("256 bits",None,data,"AES_ECB_DEC_IK"))

    def securedAES128forCBCencryptionCmd(self,iv,data):
        return(self.__securedAESgenericCmd("128 bits",iv,data,"AES_CBC_ENC_IK"))

    def securedAES256forCBCencryptionCmd(self,iv,data):
        return(self.__securedAESgenericCmd("256 bits",iv,data,"AES_CBC_ENC_IK"))

    def securedAES128forCBCdecryptionCmd(self,iv,data):
        return(self.__securedAESgenericCmd("128 bits",iv,data,"AES_CBC_DEC_IK"))

    def securedAES256forCBCdecryptionCmd(self,iv,data):
        return(self.__securedAESgenericCmd("256 bits",iv,data,"AES_CBC_DEC_IK"))

    def __securedHMACgenericCmd(self,data,tagLen,mode):
        assert(mode in ["SHA2_256_IK", "SHA2_512_IK"]), "Only compliant with 'SHA2_256_IK' and 'SHA2_512_IK'"
        assert(len(data)>0 and len(data)<=14336), " Data length must be between 1 and 14336"
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Secured Cryptography"]["HMAC_"+mode]
        tgtPayloadLen = 5 + len(data)     
        dataPayload += bytearray( struct.pack('!H', len(data) ) )
        dataPayload += bytearray( struct.pack('!H', tagLen    ) )
        dataPayload += data
        tgtAnsLen = tagLen
        cmdParams = ("Secured HMAC " + mode + " ", self._defaultTimeOut, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )

    def securedHMACSHA256genericCmd(self,data,tagLen):
        return(self.__securedHMACgenericCmd(data,tagLen,"SHA2_256_IK"))

    def securedHMACSHA512genericCmd(self,data,tagLen):
        return(self.__securedHMACgenericCmd(data,tagLen,"SHA2_512_IK"))

    def __securedMiscGeneratePasswordCmd(self,mode,size,cmd,cmdName):
        assert(cmd in ["GENERATE_CLEAR_PASSWORD", "GENERATE_ENCRYPTED_PASSWORD" ]), 'Unknown command'
        assert(mode in ['numeric','alpha numeric low case', 'alpha numeric high case', 'full']), 'Password mode is unknown'
        assert(size>0 and size<=30), "Password size must be between 1 and 30"
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Secured Misc"][cmd]                
        modeValue = {
            'numeric'                 : b'\x01',
            'alpha numeric low case'  : b'\x02',
            'alpha numeric high case' : b'\x03',
            'full'                    : b'\x04'
        }
        dataPayload += modeValue[mode]
        dataPayload += bytearray([size])        
        if(cmd == "GENERATE_CLEAR_PASSWORD"):
            tgtAnsLen = size
        cmdParams = (cmdName, self._defaultTimeOut + 2, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )

    def securedMiscGenerateClearPasswordCmd(self,mode,size):
        return( self.__securedMiscGeneratePasswordCmd(mode,size,"GENERATE_CLEAR_PASSWORD","Secured generate clear password") )

    def securedMiscGenerateEncryptedPasswordCmd(self,mode,size):
        return( self.__securedMiscGeneratePasswordCmd(mode,size,"GENERATE_ENCRYPTED_PASSWORD","Secured generate encrypted password") )
        
    def securedMiscTypeEncryptedPasswordCmd(self,data):
        assert(len(data)==56), "Input encrypted data length must be 56 length"
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Secured Misc"]["TYPE_ENCRYPTED_PASSWORD"]        
        cmdParams = ("Type Encrypted Password", self._defaultTimeOut + 3, tgtPayloadLen, tgtAnsLen)
        dataPayload += data
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )

    def securedMiscCheckFirmwareCmd(self,mode,challengeKey):
        assert(mode in [256,512]), "Valid mode are only 256 or 512"
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Secured Misc"]["CHECK_FIRMWARE"]        
        if(mode==256):
            assert(len(challengeKey)==16), "Challenge key length must be 16 byte length"
            tgtPayloadLen = 18
            tgtAnsLen = 32
            dataPayload += b'\x01'
        else:
            assert(len(challengeKey)==32), "Challenge key length must be 32 byte length"
            tgtPayloadLen = 34
            tgtAnsLen = 64
            dataPayload += b'\x02'
        dataPayload += challengeKey        
        cmdParams = ("Check firmware", self._defaultTimeOut+10, tgtPayloadLen, tgtAnsLen)        
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )


    def securedMiscGetFirmwareHashCmd(self,mode):
        assert(mode in [256,512]), "Valid mode are only 256 or 512"
        (dataPayload, tgtPayloadLen, tgtAnsLen) = self._commands["Secured Misc"]["HASH_FIRMWARE"]        
        if(mode==256):
            tgtAnsLen = 32
            dataPayload += b'\x01'
        else:
            tgtAnsLen = 64
            dataPayload += b'\x02'
        cmdParams = ("Get firmware hash", self._defaultTimeOut+10, tgtPayloadLen, tgtAnsLen)
        ( status , ret ) = self._sendFrame(cmdParams, dataPayload)
        return( status , ret )




# sudo adduser second_user dialout
