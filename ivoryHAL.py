
from abc import ABC, abstractmethod
import sys
import glob
import serial
import socket

import binascii


class IvoryGenericHAL(ABC):

    @abstractmethod
    def read(self,nBytes,timeOut=-1):
        pass

    @abstractmethod
    def write(self,txData):
        pass

    @abstractmethod
    def flush(self):
        pass

    @abstractmethod
    def isConnected(self):
        pass


class IvorySerialHAL(IvoryGenericHAL):

    def __init__(self, defaultTimeOut, verbose = True):
        self._defaultTimeOut = defaultTimeOut
        self._verbose = verbose
        portList = self._getSerialPortsList()
        if(len(portList)<1):
            raise IOError("No serial port detected")
        if(verbose):
            print("Search connected dongle:")
        for portName in portList:
            if(verbose):
                print("Seach dongle on port " + portName + " ...")
            self._serialPort = serial.Serial(
                port=portName,
                baudrate=115200,
                bytesize=8, 
                parity='N', 
                stopbits=1, 
                timeout=defaultTimeOut,
                xonxoff=0, 
                rtscts=0
            )
            nullCmd = b'\x02\x00\x01\x00\x01\x03'
            nullCmdAns = b'\x06\x02\x11\x00\x00\x00\x03'
            self._serialPort.write(nullCmd)
            ans = self._serialPort.read(len(nullCmdAns))
            if( ans == nullCmdAns):
                if(verbose):
                    print("Dongle fond on port " + portName)
                break
            else:
                self._serialPort.close()
    

    def _getSerialPortsList(self):
        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # this excludes your current terminal "/dev/tty"
            ports = glob.glob('/dev/ttyACM?')
        else:
            raise EnvironmentError('Unsupported platform')
        result = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                result.append(port)
            except (OSError, serial.SerialException):
                pass
        return result


    def __del__(self):
        try:
            if(self._serialPort.is_open):
                self._serialPort.close()
        except:
            pass

    def read(self,nBytes,timeOut=-1):
        self._checkConnexion()
        if(nBytes<1):
            return b''
        if(timeOut==-1):
            self._serialPort.timeout = self._defaultTimeOut
        else:
            self._serialPort.timeout = timeOut
        rxData = self._serialPort.read(nBytes)
        if(rxData==b''):
            raise IOError("Timeout error")
        else:
            #print(binascii.hexlify(rxData))
            return rxData

    def write(self,txData):
        self._checkConnexion()
        self._serialPort.write(txData)

    def flush(self):
        self._checkConnexion()
        self._serialPort.reset_input_buffer()
        self._serialPort.reset_output_buffer()

    def _checkConnexion(self):
        if(self._serialPort.is_open == False):
            raise IOError("Connexion closed")

    def isConnected(self):
        # check if the serial port is still connected
        try:
            self._serialPort.inWaiting()         
            return True
        except serial.serialutil.SerialException:
            #return False
            raise IOError("Connexion closed")



class IvoryTcpHAL(IvoryGenericHAL):

    def __init__(self, defaultTimeOut, listenPort, listenAddr = "0.0.0.0", verbose = True):
        self._defaultTimeOut = defaultTimeOut
        self._verbose = verbose
        # Create a TCP/IP socket
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Bind the socket to the address given on the command line
        server_address = (listenAddr, listenPort)
        self._sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) 
        self._sock.bind(server_address)
        if(verbose):
            print("Start server up on %s port %s" % self._sock.getsockname())
        self._sock.listen(1)
        if(verbose):
            print("Waiting for a connection...")
        self._connection, self._client_address = self._sock.accept()
        if(verbose):
            print("Client connected:", self._client_address)

        if(verbose):
            print("Search connected dongle...")
        #self.flush()
        nullCmd = b'\x02\x00\x01\x00\x01\x03'
        nullCmdAns = b'\x06\x02\x11\x00\x00\x00\x03'
        self._connection.sendall(nullCmd)
        ans = self._connection.recv(len(nullCmdAns))
        if( ans == nullCmdAns):
            if(verbose):
                print("Dongle detected")
        else:
            self._connection.close()
            self._sock.close()
            raise IOError("No dongle detected on TCP client connexion")

    def __del__(self):
        try:
            self._connection.close()
        except:
            pass
        try:
            self._sock.close()
        except:
            pass
        
    def read(self,nBytes,timeOut=-1):
        if(nBytes<1):
            return b''
        if(timeOut==-1):
            self._connection.settimeout(self._defaultTimeOut)
        else:
            self._connection.settimeout(timeOut)
        rxData = b''
        while(len(rxData)<nBytes):
            data = self._connection.recv(nBytes-len(rxData))
            if not data: 
                raise ConnectionAbortedError("Connexion lost")
            rxData += data
        return rxData

    def write(self,txData):
        try:
            self._connection.sendall(txData)
        except ConnectionAbortedError:
            raise ConnectionAbortedError("Connexion lost")
            
    def flush(self):
        self._connection.settimeout(0)
        try:
            _ = self._connection.recv(8192)
        except:
            pass
        self._connection.settimeout(self._defaultTimeOut)
            
    def isConnected(self):
        return True




