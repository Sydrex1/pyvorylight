#!/usr/bin python3
# coding: utf8

import ivoryAPI
import binascii
import sys
import os
import time
import system_hotkey
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
import getpass
import threading
import queue

########   Utility   ###############################################################

### Ctrl touch release detector

from pynput import keyboard
import system_hotkey

class hotkeyMonitor():
    
    def __on_release(self,key):
        try:
            if(key == key.ctrl_l or key == key.ctrl_r):
                if(self.__ctrlState == True):
                    if(self.__enabled):
                        self.callback(*self.callbackParams)
                    self.__ctrlState = False
        except:
            pass

    def __on_press(self, a, event, hotkey):
        if(self.__ctrlState == False):
            if(time.time() - self.__time < self.__tempo):
                pass
            else:
                self.__time = time.time()
                self.__ctrlState = True

    def __init__(self, hotkey, callback, callbackParams, tempo):
        self.callback = callback
        self.callbackParams = callbackParams
        self.__tempo = tempo
        keyboard.Listener(on_release= self.__on_release ).start()
        system_hotkey.SystemHotkey(consumer=self.__on_press).register(hotkey)
        self.__time = 0
        self.__ctrlState = False
        self.__enabled   = False

    def enable(self):
        self.__enabled = True
    
    def disable(self):
        self.__enabled = False

### Clear screen

def clearScreen():
    if sys.platform.startswith('win'):
        os.system('cls')
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        os.system('clear')

########   Ivory FRAM slot manager ##################################################

def readSlot(hIvory,slotIdx,verbose=False):
    slotLength = 128
    (_, ret) = hIvory.FRAMreadCmd(0,slotIdx*slotLength,slotLength)
    # Slot header 0xA4 and footer 0x77
    # Slot@1 : Index
    if(ret[0] == 0xA5 and ret[127] == 0x77 and ret[1]==slotIdx):
        # Next 56 bytes: cipher data
        cipherText = ret[2:(2+56)]
        # Up to the end-1: cipher data
        plainText  = ret[(2+56):-1]
        # Title and Info are splitted by NULL char
        [title, memo] = plainText.decode("utf-8").split('\x00')[0:2]
        ret = (slotIdx,title, memo, cipherText)
        if(verbose):
            print(binascii.hexlify(cipherText))
            print(ret)
    else:
        ret = None
    return(ret)
    
########   Main sub functions  ###############################################

def reloadItemList(hIvory):
    print("Loading password...")
    slot = []
    slotIdxList = []
    for slotIdx in range(128):
        ret = readSlot(hIvory,slotIdx)
        if(ret != None):
            slot.append(ret)
            slotIdxList.append(slotIdx)
    clearScreen()
    Ncol = 3
    idx = 0
    lineIdx = 0
    eolFlag = False
    diplayList = []
    while(eolFlag==False):
        diplayList.append("  ")
        for i in range(Ncol):
            (slotIdx,title, _, _) = slot[idx]
            diplayList[lineIdx] += f" {slotIdx: 2d} {title}".ljust(32)
            idx += 1
            if(idx >= len(slot)):
                eolFlag = True
                break
        lineIdx = lineIdx + 1     
    print("")
    for line in diplayList: 
        print(line)
    while(True):    
        print("")
        try:
            idxInpout = input("Selected item or [x] for exit : ")
        except:
            return -1
        if(idxInpout == "x" or idxInpout == "X" or hIvory.isConnected()==False):
            return -1
        if(idxInpout.isdigit()==False):
            print(" Input is not valid")
        elif(not(int(idxInpout) in slotIdxList)):
            print(" Not a valid index value")
        else:
            return int(idxInpout)

def displayItemAndWait(hIvory,idx,hCtrlMonitor):
    clearScreen()
    (_,title, memo, _) = readSlot(hIvory,idx)
    if(idx!=None):
        titlebar = "-" * (len(title) + 2)
        print(f" +{titlebar}+ ")    
        print(f" | {title} |")
        print(f" +{titlebar}+ ")    
        print("")
        if(len(memo)!=0):
            print(f" Memo : ")
            memoList = memo.split('\r')
            memoList = [ line.replace('\n','') for line in memoList ]
            for line in memoList:
                print(f"  {line}")
        print("")
        print("Ctrl+C to quit")
        hCtrlMonitor.enable()
        try:
            while(hIvory.isConnected()):
                time.sleep(1)

        except KeyboardInterrupt:
            pass
        hCtrlMonitor.disable()
    

########        MAIN        ###############################################

def main():

    # Init
    clearScreen()
    print("""
    ##########################
    # Ivory password manager #
    ##########################
    """)
    try:
        hIvory = ivoryAPI.ivoryAPI("local", verbose = True)
        hIvory.systemLEDTestCmd(0.1)
        hIvory.systemSetTimeoutCmd(30*60)        
    except OSError:
        print("")
        print("*** Ivory USB dongle not detected ***")
        print("")
        input(" Press enter...")
        sys.exit()

    print("")

    # Paswword
    digest = hashes.Hash(hashes.SHA256(), backend=default_backend())
    digest.update(getpass.getpass("Password:").encode())
    print("Password checking...")
    hIvory.securedKeyPasswordMKinitCmd(digest.finalize())
    Check_Value = bytearray.fromhex("5A5A5A5A5A5A5A5A0F0F0F0F0F0F0F0F5A5A5A5A5A5A5A5A0F0F0F0F0F0F0F0F5A5A5A5A5A5A5A5A0F0F0F0F0F0F0F0F5A5A5A5A5A5A5A5A0F0F0F0F0F0F0F0F")
    seed = bytearray.fromhex("0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF")
    label = b"CheckPassword"
    hIvory.securedKeyAesHmacKeysSetupCmd(label,seed)
    (_,ret) = hIvory.securedHMACSHA256genericCmd(Check_Value,32)
    if(ret != bytearray.fromhex("92CBB205F91DDCAAA520E7CAA8B4BC7FB34B7363F31DF426D3341FB6AC2F8E52")):
        print("*** Invalid password ***")
        print("")
        input(" Press enter...")
        sys.exit()
    else:
        print("Access granted")
        print("")

    seed = bytearray.fromhex("1EB8A5534FB548A77EC9B04B3616390C3492D1AFC70D1F46AAF591FA0956B1A3")
    label = b"WPFPasswordDesktopManager"
    hIvory.securedKeyAesHmacKeysSetupCmd(label,seed)

    
    #Password sender thread
    
    def sendPasswordTask(localIvory, sendPasswordQueue):
        isRunning = True
        while(isRunning):
            (isRunning,cipherText) = sendPasswordQueue.get()
            if(isRunning == True):
                if(hIvory.isConnected()):
                    _ = localIvory.securedMiscTypeEncryptedPasswordCmd(cipherText)

    sendPasswordQueue = queue.Queue()
    sendPasswordThread = threading.Thread(target=sendPasswordTask, args=(hIvory, sendPasswordQueue ))
    sendPasswordThread.start()

    # Set up keyboard and ctrl monitor

    def typePassword(localIvory, sendPasswordQueue):
        if(hIvory.isConnected()):
            ret = readSlot(localIvory,selectedIdx)
        else:
           ret=None 
        if(ret!=None):
            ( _, _, _, cipherText ) = ret
            sendPasswordQueue.put((True,cipherText))
            
    hIvory.keyboardSetModeCmd("azerty")
    hCtrlMonitor = hotkeyMonitor(('control', 'space'),typePassword ,(hIvory,sendPasswordQueue),2)

    try:
        if( len(sys.argv) == 2):
            if( sys.argv[1].isdigit()):
                selectedIdx = int(sys.argv[1])
                displayItemAndWait(hIvory,selectedIdx,hCtrlMonitor)
        
        # Main loop
        while(hIvory.isConnected()):
            selectedIdx = reloadItemList(hIvory)
            if(selectedIdx==-1):
                clearScreen()
                break
            displayItemAndWait(hIvory,selectedIdx,hCtrlMonitor)
            clearScreen()
    except IOError:
        clearScreen()
    finally:
        sendPasswordQueue.put((False,None))
        sendPasswordThread.join()

if __name__== "__main__":
    main()
    
